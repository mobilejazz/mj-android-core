package com.mobilejazz.datasource.api.helper.okHttp;

import android.content.Context;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;

import java.io.*;
import java.util.*;

public class OkHttpFactory {

  private static final int CACHE_SIZE = 50 * 1024 * 1024;

  private OkHttpFactory() {
    throw new AssertionError("No instances of this object are allowed!");
  }

  public static OkHttpClient create(Context context) {
    OkHttpClient client = new OkHttpClient();

    File cacheDirectory = new File(context.getCacheDir().getAbsolutePath(), "HttpCache");
    Cache cache = new Cache(cacheDirectory, CACHE_SIZE);

    client.setCache(cache);

    return client;
  }

  public static OkHttpClient create(Context context, Interceptor interceptor) {
    OkHttpClient client = create(context);
    client.interceptors().add(interceptor);
    return client;
  }

  public static OkHttpClient create(Context context, List<Interceptor> interceptors) {
    OkHttpClient client = create(context);

    for (Interceptor interceptor : interceptors) {
      client.interceptors().add(interceptor);
    }

    return client;
  }
}
