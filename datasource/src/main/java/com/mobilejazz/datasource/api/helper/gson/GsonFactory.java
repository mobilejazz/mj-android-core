package com.mobilejazz.datasource.api.helper.gson;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;

public class GsonFactory {

  private GsonFactory() {
    throw new AssertionError("No instances of this class are allowed");
  }

  @Deprecated public static GsonBuilder create() {
    return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setPrettyPrinting()
        .registerTypeAdapter(IsoDateConverter.TYPE, new IsoDateConverter());
  }

  public static GsonBuilder generate() {
    return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setPrettyPrinting()
        .registerTypeAdapter(IsoDateConverter.TYPE, new IsoDateConverter());
  }
}
