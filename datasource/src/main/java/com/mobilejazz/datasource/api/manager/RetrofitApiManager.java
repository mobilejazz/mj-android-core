package com.mobilejazz.datasource.api.manager;

import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.mobilejazz.datasource.api.helper.gson.GsonFactory;
import com.squareup.okhttp.OkHttpClient;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

import java.util.concurrent.*;

public class RetrofitApiManager {

  static final String THREAD_PREFIX = "Retrofit-";
  static final String IDLE_THREAD_NAME = THREAD_PREFIX + "Idle";

  private RestAdapter restAdapter;
  private boolean isDebug;
  private String address;

  private OkHttpClient okHttpClient;
  private Gson gson;

  private Executor httpExecutor;
  private Executor callbackExecutor;

  /**
   * Default constructor
   */
  public RetrofitApiManager() {
  }

  public static Executor createHttpExecutor() {
    return Executors.newCachedThreadPool(new ThreadFactory() {
      @Override public Thread newThread(@NonNull final Runnable r) {
        return new Thread(new Runnable() {
          @Override public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            r.run();
          }
        }, IDLE_THREAD_NAME);
      }
    });
  }

  /**
   * Set the {@link RestAdapter} log level.
   *
   * @param isDebug If true, the log level is set to {@link RestAdapter.LogLevel#FULL}.
   * Otherwise {@link RestAdapter.LogLevel#NONE}.
   */
  public RetrofitApiManager setIsDebug(boolean isDebug) {
    this.isDebug = isDebug;
    if (restAdapter != null) {
      restAdapter.setLogLevel(isDebug ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE);
    }
    return this;
  }

  public RetrofitApiManager setAddress(String address) {
    if (address == null) {
      throw new IllegalArgumentException("Address must be not null");
    }

    this.address = address;

    return this;
  }

  public RetrofitApiManager setOkHttpClient(OkHttpClient okHttpClient) {
    if (okHttpClient == null) {
      throw new IllegalArgumentException("OkHttpClient must be not null");
    }

    this.okHttpClient = okHttpClient;
    return this;
  }

  public RetrofitApiManager setGson(Gson gson) {
    if (gson == null) {
      throw new IllegalArgumentException("Gson must be not null");
    }

    this.gson = gson;
    return this;
  }

  public RetrofitApiManager setExecutors(Executor httpExecutor,
      @Nullable Executor callbackExecutor) {
    if (httpExecutor == null) {
      throw new IllegalArgumentException("httpExecutor must not be null!");
    }

    this.httpExecutor = httpExecutor;
    this.callbackExecutor = callbackExecutor;

    return this;
  }

  /**
   * Create a new {@link RestAdapter.Builder}. Override this to e.g. set your own client
   * or
   * executor.
   *
   * @return A {@link RestAdapter.Builder} with no modifications.
   */
  protected RestAdapter.Builder newRestAdapterBuilder() {
    return new RestAdapter.Builder();
  }

  /**
   * Return the current {@link RestAdapter} instance. If none exists builds a new one.
   */
  protected RestAdapter getRestAdapter() {
    if (checkNotNull(address)) {
      throw new NullPointerException("Address must be not null");
    }

    if (restAdapter == null) {
      RestAdapter.Builder builder = newRestAdapterBuilder();
      builder.setEndpoint(address);

      if (isDebug) {
        builder.setLogLevel(RestAdapter.LogLevel.FULL);
      }

      builder.setClient(new OkClient(okHttpClient));

      Gson gson = getGson();
      builder.setConverter(new GsonConverter(gson));

      if (httpExecutor != null) {
        builder.setExecutors(httpExecutor, callbackExecutor);
      }

      restAdapter = builder.build();
    }

    return restAdapter;
  }

  @NonNull private Gson getGson() {
    Gson gson;
    if (this.gson == null) {
      gson = GsonFactory.generate().create();
    } else {
      gson = this.gson;
    }
    return gson;
  }

  private boolean checkNotNull(String value) {
    return value == null || value.length() == 0;
  }
}
