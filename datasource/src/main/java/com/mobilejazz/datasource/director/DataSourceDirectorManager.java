package com.mobilejazz.datasource.director;

public class DataSourceDirectorManager implements DataSourceDirector {

  private Policy policy;

  public DataSourceDirectorManager() {
    this.policy = Policy.EMPTY;
  }

  @Override public <T> T performSync(final NetworkSync<T> network, final CacheSync<T> cache) {
    final DataSourceDirectorResolver<T> resolver = new DataSourceDirectorResolver<>();

    resolver.initialize(new DataSourceDirectorResolver.Callback<T>() {
      @Override public T resolve(DataSourceDirectorResolver.Route route) {
        switch (route) {
          case CACHE:

            if (cache != null) {
              return cache.execute(resolver);
            } else {
              throw new IllegalStateException("Cache interface must be not null");
            }

          case NETWORK:

            if (network != null) {
              return network.execute(resolver);
            } else {
              throw new IllegalStateException("Network interface must be not null");
            }

          default:
            throw new IllegalStateException("Undefined route");
        }
      }
    });

    if (policy == Policy.FORCE_REFRESH) {
      // If force refresh, use network block
      if (network != null) {
        // If network block, execute network block
        return resolver.resolve(DataSourceDirectorResolver.Route.NETWORK);
      } else {
        // Nothing to be done. No block is executed.
        throw new IllegalStateException("Network<T> is null");
      }
    } else if (policy == Policy.EMPTY) {
      // Otherwise, use cache block.
      if (cache != null) {
        // If cache block is defined, execute cache block
        return resolver.resolve(DataSourceDirectorResolver.Route.CACHE);
      } else {
        // Otherwise, resolve cache block with error
        return resolver.resolve(DataSourceDirectorResolver.Route.NETWORK);
      }
    } else {
      throw new IllegalStateException("Policy not defined");
    }
  }

  @Override public <T> void performAsync(final NetworkAsync<T> network, final CacheAsync<T> cache) {
    final DataSourceDirectorResolver<T> resolver = new DataSourceDirectorResolver<>();
    resolver.initializeAsync(new DataSourceDirectorResolver.CallbackAsync() {
      @Override public void resolve(DataSourceDirectorResolver.Route route) {
        switch (route) {
          case CACHE:

            if (cache != null) {
              cache.execute(resolver);
            } else {
              throw new IllegalStateException("Cache interface must be not null");
            }

          case NETWORK:

            if (network != null) {
              network.execute(resolver);
            } else {
              throw new IllegalStateException("Network interface must be not null");
            }

            break;

          default:
            throw new IllegalStateException("Undefined route");
        }

      }
    });

    if (policy == Policy.FORCE_REFRESH) {
      // If force refresh, use network block
      if (network != null) {
        // If network block, execute network block
        resolver.resolveAsync(DataSourceDirectorResolver.Route.NETWORK);
      } else {
        // Nothing to be done. No block is executed.
        throw new IllegalStateException("Network<T> is null");
      }
    } else if (policy == Policy.EMPTY) {
      // Otherwise, use cache block.
      if (cache != null) {
        // If cache block is defined, execute cache block
        resolver.resolveAsync(DataSourceDirectorResolver.Route.CACHE);
      } else {
        // Otherwise, resolve cache block with error
        resolver.resolveAsync(DataSourceDirectorResolver.Route.NETWORK);
      }
    } else {
      throw new IllegalStateException("Policy not defined");
    }
  }

  @Override public void changePolicy(Policy policy) {
    this.policy = policy;
  }

  @Override public void resetPolicy() {
    policy = Policy.EMPTY;
  }

  //region Private methods

  //endregion
}
