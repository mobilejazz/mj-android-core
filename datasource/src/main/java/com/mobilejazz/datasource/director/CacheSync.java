package com.mobilejazz.datasource.director;

public interface CacheSync<T> extends Cache {

  T execute(DataSourceDirectorResolver<T> resolver);
}
