package com.mobilejazz.datasource.director;

public interface CacheAsync<T> extends Cache {

  void execute(DataSourceDirectorResolver<T> resolver);
}
