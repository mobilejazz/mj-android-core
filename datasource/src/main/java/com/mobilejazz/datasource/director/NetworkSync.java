package com.mobilejazz.datasource.director;

public interface NetworkSync<T> extends Network {

  T execute(DataSourceDirectorResolver<T> resolver);
}
