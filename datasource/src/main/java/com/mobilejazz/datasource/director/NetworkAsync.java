package com.mobilejazz.datasource.director;

public interface NetworkAsync<T> extends Network {

  void execute(DataSourceDirectorResolver<T> resolver);
}
