package com.mobilejazz.datasource.director;

/**
 * A data provider director executes cache and network routes.
 **/
public interface DataSourceDirector {

  enum Policy {
    FORCE_REFRESH,
    EMPTY
  }

  <T> T performSync(NetworkSync<T> network, CacheSync<T> cache);

  <T> void performAsync(NetworkAsync<T> network, CacheAsync<T> cache);

  void changePolicy(Policy policy);

  void resetPolicy();
}
