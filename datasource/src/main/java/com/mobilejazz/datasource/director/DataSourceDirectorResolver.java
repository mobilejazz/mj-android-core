package com.mobilejazz.datasource.director;

public class DataSourceDirectorResolver<T> {

  private CallbackAsync callbackAsync;

  public enum Route {
    CACHE,
    NETWORK
  }

  interface Callback<T> {

    T resolve(Route route);
  }

  interface CallbackAsync {

    void resolve(Route route);
  }

  private Callback<T> callback;

  public void initialize(Callback<T> callback) {
    this.callback = callback;
  }

  public void initializeAsync(CallbackAsync callbackAsync) {
    this.callbackAsync = callbackAsync;
  }

  public T resolve(Route route) {
    if (callback != null) {
      return callback.resolve(route);
    } else {
      throw new IllegalStateException("Callback is null");
    }
  }

  public void resolveAsync(Route route) {
    if (callbackAsync != null) {
      callbackAsync.resolve(route);
    } else {
      throw new IllegalStateException("CallbackAsync is null");
    }
  }
}
