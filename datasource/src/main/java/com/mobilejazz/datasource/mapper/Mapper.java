package com.mobilejazz.datasource.mapper;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Interface with the public contract that all the mapper classes need to implement.
 *
 * @param <I> Input Object Class
 * @param <O> Output Object Class
 */
public interface Mapper<I, O> {

  @NonNull O transform(@NonNull final I value);

  List<O> transform(@NonNull final List<I> value);
}