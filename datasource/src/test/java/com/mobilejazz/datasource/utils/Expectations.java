package com.mobilejazz.datasource.utils;

import java.util.concurrent.*;

public class Expectations {

  private static CountDownLatch lock;

  public static void initialize() {
    lock = new CountDownLatch(1);
  }

  public static void release() {
    lock.countDown();
  }

  public static void await(long value, TimeUnit unit) {
    try {
      lock.await(value, unit);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
