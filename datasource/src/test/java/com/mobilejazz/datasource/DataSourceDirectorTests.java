package com.mobilejazz.datasource;

import com.mobilejazz.datasource.director.CacheAsync;
import com.mobilejazz.datasource.director.CacheSync;
import com.mobilejazz.datasource.director.DataSourceDirector;
import com.mobilejazz.datasource.director.DataSourceDirectorManager;
import com.mobilejazz.datasource.director.DataSourceDirectorResolver;
import com.mobilejazz.datasource.director.NetworkAsync;
import com.mobilejazz.datasource.director.NetworkSync;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class DataSourceDirectorTests {

  private DataSourceDirector director;

  @Before public void setUp() throws Exception {
    director = new DataSourceDirectorManager();
  }

  @Test
  public void should_call_to_the_network_and_not_to_the_cache_when_policy_force_with_async_director()
      throws Exception {
    CacheAsync<Object> cacheAsyncMock = (CacheAsync<Object>) mock(CacheAsync.class);
    NetworkAsync<Object> networkAsyncMock = (NetworkAsync<Object>) mock(NetworkAsync.class);

    director.changePolicy(DataSourceDirector.Policy.FORCE_REFRESH);

    director.performAsync(networkAsyncMock, cacheAsyncMock);

    verify(networkAsyncMock, times(1)).execute(any(DataSourceDirectorResolver.class));
    verify(cacheAsyncMock, times(0)).execute(any(DataSourceDirectorResolver.class));
  }

  @Test public void should_call_to_network_and_cache_when_policy_empty_with_async_director()
      throws Exception {
    CacheAsync<Object> cacheAsyncMock = (CacheAsync<Object>) mock(CacheAsync.class);
    NetworkAsync<Object> networkAsyncMock = (NetworkAsync<Object>) mock(NetworkAsync.class);

    director.changePolicy(DataSourceDirector.Policy.EMPTY);

    director.performAsync(networkAsyncMock, cacheAsyncMock);

    verify(networkAsyncMock, times(1)).execute(any(DataSourceDirectorResolver.class));
    verify(cacheAsyncMock, times(1)).execute(any(DataSourceDirectorResolver.class));
  }

  @Ignore @Test public void should_call_to_network_and_cache_when_policy_empty_with_sync_director()
      throws Exception {
    CacheSync<Object> cacheSyncMock = (CacheSync<Object>) mock(CacheSync.class);
    NetworkSync<Object> networkSyncMock = (NetworkSync<Object>) mock(NetworkSync.class);

    director.changePolicy(DataSourceDirector.Policy.EMPTY);

    Object object = director.performSync(networkSyncMock, cacheSyncMock);

    verify(networkSyncMock, times(1)).execute(any(DataSourceDirectorResolver.class));
    verify(cacheSyncMock, times(1)).execute(any(DataSourceDirectorResolver.class));
  }

  @Test public void should_response_a_object_from_cache_if_policy_is_empty_with_sync_director()
      throws Exception {
    director.changePolicy(DataSourceDirector.Policy.EMPTY);
    User user = director.performSync(new NetworkSync<User>() {
      @Override public User execute(DataSourceDirectorResolver<User> resolver) {
        return null;
      }
    }, new CacheSync<User>() {
      @Override public User execute(DataSourceDirectorResolver<User> resolver) {
        return new User(1, "jose");
      }
    });

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1);
    assertThat(user.getName()).isEqualToIgnoringCase("jose");
  }

  @Test
  public void should_response_object_from_network_if_resolver_go_to_network_with_sync_director()
      throws Exception {
    director.changePolicy(DataSourceDirector.Policy.EMPTY);
    User user = director.performSync(new NetworkSync<User>() {
      @Override public User execute(DataSourceDirectorResolver<User> resolver) {
        return new User(1, "jose");
      }
    }, new CacheSync<User>() {
      @Override public User execute(DataSourceDirectorResolver<User> resolver) {
        return resolver.resolve(DataSourceDirectorResolver.Route.NETWORK);
      }
    });

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1);
    assertThat(user.getName()).isEqualToIgnoringCase("jose");
  }

  @Test
  public void should_response_object_from_network_if_policy_is_force_refresh_with_sync_director()
      throws Exception {
    director.changePolicy(DataSourceDirector.Policy.FORCE_REFRESH);
    User user = director.performSync(new NetworkSync<User>() {
      @Override public User execute(DataSourceDirectorResolver<User> resolver) {
        return new User(1, "jose");
      }
    }, new CacheSync<User>() {
      @Override public User execute(DataSourceDirectorResolver<User> resolver) {
        return null;
      }
    });

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1);
    assertThat(user.getName()).isEqualToIgnoringCase("jose");
  }

  class User {

    private int id;
    private String name;

    public User(int id, String name) {
      this.id = id;
      this.name = name;
    }

    public int getId() {
      return id;
    }

    public String getName() {
      return name;
    }
  }
}