package com.mobilejazz.application.presenter;

import com.mobilejazz.common.error.ErrorCore;

/**
 * Interface representing a BaseView which a {@link Presenter} must be bound.
 */
public interface BaseView {

  void displayError(ErrorCore<?> error);

  void hideError();

  void displayProgressView();

  void hideProgressView();
}
