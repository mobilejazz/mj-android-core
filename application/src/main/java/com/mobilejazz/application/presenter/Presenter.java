package com.mobilejazz.application.presenter;

/**
 * Interface representing a Presenter in a model view presenter (MVP) pattern.
 */
public interface Presenter<T extends BaseView> {

  /**
   * Method that control the initialization of the view. It should be called in the onCreate() or
   * onCreatedView()
   */
  void initialize();

  /**
   * Method that control the lifecycle of the view. It should be called in the view's
   * (Activity or Fragment) onResume() method.
   */
  void onResume();

  /**
   * Method that control the lifecycle of the view. It should be called in the view's
   * (Activity or Fragment) onPause() method.
   */
  void onPause();

  /**
   * Method that control the lifecycle of the view. It should be called in the view's
   * (Activity or Fragment) onDestroy() method.
   */
  void onDestroy();

  /**
   * Method that sets the view responsible of implement the contract.
   */
  void setView(T view);
}