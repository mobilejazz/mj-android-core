![Mobile Jazz Vastra](https://raw.githubusercontent.com/mobilejazz/metadata/master/images/banners/mobile-jazz-android-core.jpg)

MJ-Android-Core
=============

A Mobile Jazz Android library project to create a standardized way of developing android architectures.

Add it to your project
-------------------------------

### Repository

First, add the following to your app's `build.gradle` file:

```Gradle
repositories {
   maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
}
```

### Library

```gradle
dependencies {

    // ... other dependencies here

    compile 'com.mobilejazz.android-core:library:2.0.5-SNAPSHOT'
}
```

Documentation
-------------------------------

### 1. Application Structure

If we don't think that the application needs a special structure, the applications that we create from scratch will have the following rules in terms of the project structure:

* The project will just have one **module** and the name could be `app` or `application`
* The parent package structure will be `com.{COMPANY_NAME}.{APPLICATION_NAME}` and it will be separated in three big groups / packages **application**, **domain** and **datasource**. The result must be something like this:
    * `com.{COMPANY_NAME}.{APPLICATION_NAME}.application`
    * `com.{COMPANY_NAME}.{APPLICATION_NAME}.domain`
    * `com.{COMPANY_NAME}.{APPLICATION_NAME}.dataprovider`

### 2. Architecture

All the application is based in three big parts **application**, **domain** and **dataprovider** and each one has their own reponsibility.

#### 2.1 Application

To do

##### 2.1.1 Dependency injection

To do

##### 2.1.2 Model View Presenter

To do

##### 2.1.3 Error management

To do

#### 2.2 Domain

The domain contains all the business logic of the application based in **interactors**.

##### 2.2.1 Interactors

To do

##### 2.2.2 Threading

To do

#### 2.3 DataProvider

The DataProvider contains all the repositories where the information is provided for the **interactors**.

##### 2.3.1 Repositories

All the repositories must to implement the following interfaces:

* Parent Repository implements `Repository`, for example, [CharacterDataProvider](https://bitbucket.org/mobilejazz/mj-android-app-documentation/src/3a7ec411efd6a28df15862a12d4a3d27c559684a/app/src/main/java/com/mobilejazz/doc/dataprovider/CharacterDataProvider.java?fileviewer=file-view-default#CharacterDataProvider.java-13)
* Network Repository implements `Repository.Network`, for example, [CharacterNetworkDataProvider](https://bitbucket.org/mobilejazz/mj-android-app-documentation/src/3a7ec411efd6a28df15862a12d4a3d27c559684a/app/src/main/java/com/mobilejazz/doc/dataprovider/network/CharacterNetworkDataProvider.java?fileviewer=file-view-default#CharacterNetworkDataProvider.java-10:11)
* Storage Repository implements `Repository.Storage`, for example, [CharacterStorageDataProvider](https://bitbucket.org/mobilejazz/mj-android-app-documentation/src/43078585babdd35e6a0c47fd1e54a29ff52562c3/app/src/main/java/com/mobilejazz/doc/dataprovider/storage/CharacterStorageDataProvider.java?fileviewer=file-view-default#CharacterStorageDataProvider.java-10:11)

The repositories need to have a standard naming and it's defined as `{MODEL_NAME} + {RESPONSABILITY} + DataProvider`. You can follow the same pattern as the **mj-android-app-documentation** project.

If you need some method that the `Repository` interface doesn't provide, you just need to create another interface and extends from the parent `Repository` / `Repository.Storage` / `Repository.Network` interface and add theses methods that you need.

##### 2.3.2 Mappers

The responsibility of the mappers is map a object from a type to another type and their implement the interface `Mapper<From, To>` that contains a method that needs to be implemented to transform the object.

To have naming consistency in all the mappers classes that there are a lot of classes. The naming structure is `{MODEL_NAME_FROM} + To {MODEN_NAME_TO} + MAPPER `.

Examples:
* [CharacterEntityToCharacterMapper](https://bitbucket.org/mobilejazz/mj-android-app-documentation/src/43078585babdd35e6a0c47fd1e54a29ff52562c3/app/src/main/java/com/mobilejazz/doc/dataprovider/mapper/CharacterEntityToCharacterMapper.java?fileviewer=file-view-default)
* [CharacterEntityListToCharacterListMapper](https://bitbucket.org/mobilejazz/mj-android-app-documentation/src/7488ce754aa26623dd9e5fe3d0247a81b0ec0207/app/src/main/java/com/mobilejazz/doc/dataprovider/mapper/CharacterEntityListToCharacterListMapper.java?fileviewer=file-view-default)

##### 2.3.3 Networking

The libraries decided to use for the networking logic are:

* [GSON](https://github.com/google/gson) (v2.4)
* [Retrofit 2](https://github.com/square/retrofit) (v2.1.0)
* [OkHttp 3](https://github.com/square/okhttp)  (v3.4.1)

The core already contains some classes to build and initialize the required instances for the libraries.

Examples:
* TO DO

##### 2.3.3 Storage

The libraries decided to use for the storage and caching logic are:

* [CacheIO](https://github.com/mobilejazz/CacheIO) (Used just for caching purposes)
* [Realm](https://realm.io/docs/java/latest/) (Used for a full storage with queries, transactions, etc..)

Examples:
* TO DO

License
-------

    Copyright 2015 Mobile Jazz

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
