package com.mobilejazz.domain.executor;

import com.mobilejazz.domain.Interactor;
import com.mobilejazz.logger.library.Logger;

import javax.inject.Inject;
import java.util.concurrent.*;

public class SingleThreadExecutor extends BaseThreadExecutor implements InteractorExecutor {

  private static final int CORE_POOL_SIZE = 1;
  private static final int MAX_POOL_SIZE = 1;
  private static final int KEEP_ALIVE_TIME = 0;
  private static final TimeUnit TIME_UNIT = TimeUnit.MILLISECONDS;
  private static final BlockingQueue<Runnable> WORK_QUEUE = new LinkedBlockingQueue<>();

  private final ThreadPoolExecutor threadPoolExecutor;

  @Inject public SingleThreadExecutor(final Logger logger) {
    super(logger);
    threadPoolExecutor =
        new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TIME_UNIT,
            WORK_QUEUE, new InteractorThreadFactory(logger)) {
          @Override protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            promiseAfterExecute(r, t);
          }
        };
  }

  @Override public void run(final Interactor interactor) {
    if (interactor == null) {
      throw new IllegalArgumentException("Interactor must not be null");
    }

    threadPoolExecutor.submit(new Runnable() {
      @Override public void run() {
        interactor.run();
      }
    });
  }
}
