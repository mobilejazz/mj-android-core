package com.mobilejazz.domain.executor;

import com.mobilejazz.domain.Interactor;

public interface InteractorExecutor {
  void run(Interactor interactor);
}
