package com.mobilejazz.domain.executor;

import com.mobilejazz.logger.library.Logger;

import java.io.*;
import java.util.concurrent.*;

class BaseThreadExecutor {

  private static final String TAG = BaseThreadExecutor.class.getSimpleName();

  private final Logger logger;

  public BaseThreadExecutor(Logger logger) {
    this.logger = logger;
  }

  protected void promiseAfterExecute(Runnable r, Throwable t) {
    if (t == null && r instanceof Future<?>) {
      try {
        Future<?> future = (Future<?>) r;
        if (future.isDone()) {
          future.get();
        }
      } catch (CancellationException | ExecutionException ce) {
        t = ce.getCause();
      } catch (InterruptedException ie) {
        t = ie.getCause();
        Thread.currentThread().interrupt();
      } finally {
        if (t != null) {
          StringWriter writer = new StringWriter();
          PrintWriter printWriter = new PrintWriter(writer);
          t.printStackTrace(printWriter);
          printWriter.flush();
          logger.e(TAG, writer.toString());
        }
      }
    } else if (t != null) {
      logger.e(TAG, t.getMessage());
    }
  }
}
