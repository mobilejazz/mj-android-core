package com.mobilejazz.domain;

public interface Interactor {
  void run();
}
