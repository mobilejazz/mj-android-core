package com.mobilejazz.domain;

interface BaseDomainCallback<T, E> {

  void onSuccess(T result);

  void onError(E error);
}
