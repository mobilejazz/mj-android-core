package com.mobilejazz.common.callback;

import com.mobilejazz.common.error.ErrorCore;

@Deprecated public interface CompletionCallback<T> {
  void onSuccess(T result);

  void onError(ErrorCore error);
}
