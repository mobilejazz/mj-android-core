package com.mobilejazz.common.callback;

import com.mobilejazz.common.error.ErrorCore;

public interface Callback<T> {

  void onSuccess(T result);

  void onError(ErrorCore<?> error);
}
