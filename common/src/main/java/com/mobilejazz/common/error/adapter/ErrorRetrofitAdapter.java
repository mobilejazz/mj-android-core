package com.mobilejazz.common.error.adapter;

import com.mobilejazz.common.error.ErrorCore;
import com.mobilejazz.common.error.exception.NetworkErrorException;
import com.mobilejazz.logger.library.Logger;
import retrofit.RetrofitError;

public final class ErrorRetrofitAdapter implements ErrorAdapter<RetrofitError> {

  private static final String TAG = ErrorRetrofitAdapter.class.getSimpleName();

  private Logger logger;

  public ErrorRetrofitAdapter(Logger logger) {
    this.logger = logger;
  }

  @Override public ErrorCore<?> of(RetrofitError error) {
    logger.e(TAG, error.toString());

    switch (error.getKind()) {
      case CONVERSION:
        return ErrorCore.of(NetworkErrorException.of(NetworkErrorException.ErrorType.CONVERSION),
            NetworkErrorException.ErrorType.CONVERSION.getErrorCause());
      case HTTP:
        return ErrorCore.of(NetworkErrorException.of(NetworkErrorException.ErrorType.HTTP),
            NetworkErrorException.ErrorType.HTTP.getErrorCause());
      case NETWORK:
        return ErrorCore.of(NetworkErrorException.of(NetworkErrorException.ErrorType.NETWORK),
            NetworkErrorException.ErrorType.NETWORK.getErrorCause());
      default:
        return ErrorCore.of(NetworkErrorException.of(NetworkErrorException.ErrorType.UNEXPECTED),
            NetworkErrorException.ErrorType.UNEXPECTED.getErrorCause());
    }
  }
}
