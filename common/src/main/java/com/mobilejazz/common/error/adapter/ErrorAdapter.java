package com.mobilejazz.common.error.adapter;

import com.mobilejazz.common.error.ErrorCore;

public interface ErrorAdapter<T> {

  ErrorCore<?> of(T error);
}
