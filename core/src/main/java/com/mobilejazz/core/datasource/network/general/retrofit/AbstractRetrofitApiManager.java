package com.mobilejazz.core.datasource.network.general.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

import java.lang.annotation.Annotation;

public abstract class AbstractRetrofitApiManager implements RetrofitApiManager {

  private Retrofit retrofit;

  @Override public <S> S createService(Class<S> serviceClass) {
    if (retrofit == null) {
      retrofit = createRetrofit();
    }
    return retrofit.create(serviceClass);
  }

  public <T> Converter<ResponseBody, T> responseBodyConverter(Class<T> classz) {
    if (retrofit == null) {
      throw new IllegalArgumentException("You must to initialize the Retrofit instance");
    }

    return retrofit.responseBodyConverter(classz, new Annotation[0]);
  }

  protected abstract Retrofit createRetrofit();

}
