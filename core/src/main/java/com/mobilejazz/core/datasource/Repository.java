package com.mobilejazz.core.datasource;

import com.mobilejazz.core.common.Callback;
import com.mobilejazz.core.datasource.model.RepositoryModel;
import com.mobilejazz.core.datasource.spec.RepositorySpecification;
import com.google.common.base.Optional;

import java.util.*;

public interface Repository<M extends RepositoryModel, S extends RepositorySpecification> {

  void get(S specification, Callback<Optional<M>> callback);

  void getAll(S specification, Callback<Optional<List<M>>> callback);

  void put(M m, S specification, Callback<Optional<M>> callback);

  void putAll(Collection<M> ms, S specification, Callback<Optional<List<M>>> callback);

  void remove(M m, S specification, Callback<Optional<M>> callback);

  void removeAll(Collection<M> ms, S specification, Callback<Optional<List<M>>> callback);

  interface Storage<M extends RepositoryModel, S extends RepositorySpecification>
      extends Repository<M, S> {

  }

  interface Network<M extends RepositoryModel, S extends RepositorySpecification>
      extends Repository<M, S> {

  }
}
