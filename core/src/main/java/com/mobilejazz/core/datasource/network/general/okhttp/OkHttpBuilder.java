package com.mobilejazz.core.datasource.network.general.okhttp;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import okhttp3.Cache;
import okhttp3.ConnectionPool;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import java.io.File;

public class OkHttpBuilder {

  public enum LogLevel {
    NONE(HttpLoggingInterceptor.Level.NONE),
    BASIC(HttpLoggingInterceptor.Level.BASIC),
    HEADERS(HttpLoggingInterceptor.Level.HEADERS),
    BODY(HttpLoggingInterceptor.Level.BODY);

    private HttpLoggingInterceptor.Level level;

    LogLevel(HttpLoggingInterceptor.Level level) {
      this.level = level;
    }

    HttpLoggingInterceptor.Level toInterceptorLevel() {
      return level;
    }
  }

  private OkHttpClient.Builder delegate;
  private boolean enableLog;
  private LogLevel loglevel;
  private HttpLoggingInterceptor loggingInterceptor;

  public static ConnectionPool createDefaultConnectionPool() {
    return new ConnectionPool();
  }

  public OkHttpBuilder() {
    delegate = new OkHttpClient.Builder();
  }

  public OkHttpBuilder cache(Cache cache) {
    delegate.cache(cache);
    return this;
  }

  public OkHttpBuilder addInterceptor(@NonNull final Interceptor interceptor) {
    if (interceptor instanceof HttpLoggingInterceptor) {
      throw new IllegalArgumentException("Enable logging using the logging method");
    }
    delegate.addInterceptor(interceptor);
    return this;
  }

  public OkHttpBuilder retryOnConnectionFailure(boolean retry) {
    delegate.retryOnConnectionFailure(retry);
    return this;
  }

  public OkHttpBuilder connectionPool(final ConnectionPool connectionPool) {
    delegate.connectionPool(connectionPool);
    return this;
  }

  public OkHttpBuilder setHostnameVerifier(final HostnameVerifier hostnameVerifier) {
    delegate.hostnameVerifier(hostnameVerifier);
    return this;
  }

  public <T extends HttpLoggingInterceptor> OkHttpBuilder logging(final boolean enable, LogLevel loglevel, final T interceptor) {
    this.enableLog = true;
    this.loglevel = loglevel;
    this.loggingInterceptor = interceptor;
    return this;
  }

  public OkHttpClient build() {
    if (enableLog) {
      loggingInterceptor.setLevel(loglevel.toInterceptorLevel());
      delegate.addInterceptor(loggingInterceptor);
    }

    return delegate.build();
  }

  public static class CacheBuilder {

    public static final String DEFAULT_CACHE_NAME = "HttpCache";
    public static final int DEFAULT_CACHE_SIZE = 50 * 1024 * 1024; // 50 Mb

    private String cacheName;
    private String cachePath;
    private int cacheSize;

    public CacheBuilder path(@NonNull final String path) {
      if (TextUtils.isEmpty(path)) {
        throw new IllegalArgumentException("path is empty or null");
      }
      this.cachePath = path;
      return this;
    }

    public CacheBuilder name(@NonNull final String name) {
      if (TextUtils.isEmpty(name)) {
        throw new IllegalArgumentException("name is empty or null");
      }
      this.cacheName = name;
      return this;
    }

    public CacheBuilder size(final int size) {
      if (size <= 0) {
        throw new IllegalArgumentException("size must be higher than 0");
      }
      this.cacheSize = size;
      return this;
    }

    public Cache build() {
      final File path = new File(this.cachePath, this.cacheName);
      return new Cache(path, this.cacheSize);
    }

  }

  // Used mainly for debugging purposes
  public static class AcceptAllHostnamesVerifier implements HostnameVerifier {

    @Override public boolean verify(String hostname, SSLSession session) {
      return true;
    }
  }

}
