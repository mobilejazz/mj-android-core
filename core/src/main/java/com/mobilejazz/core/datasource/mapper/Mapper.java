package com.mobilejazz.core.datasource.mapper;

public interface Mapper<From, To> {

  To map(From from);
}
