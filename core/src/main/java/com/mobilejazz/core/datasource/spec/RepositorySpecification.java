package com.mobilejazz.core.datasource.spec;

public abstract class RepositorySpecification {

  public abstract String getIdentifier();

  public static class NoneSpecification extends RepositorySpecification {

    @Override public String getIdentifier() {
      return "none";
    }
  }

}
