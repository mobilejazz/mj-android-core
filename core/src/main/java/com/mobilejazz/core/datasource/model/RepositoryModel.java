package com.mobilejazz.core.datasource.model;

public abstract class RepositoryModel {

  public abstract String getIdentifier();

}
