package com.mobilejazz.core.domain.interactor;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.*;

@Deprecated
public interface InteractorHandler {

  <T> void addCallbackMainThread(ListenableFuture<T> listenableFuture, FutureCallback<T> callback);

  <T> void addCallback(ListenableFuture<T> listenableFuture, FutureCallback<T> callback);

  <T> void addCallback(ListenableFuture<T> listenableFuture, FutureCallback<T> callback,
      Executor executor);
}
