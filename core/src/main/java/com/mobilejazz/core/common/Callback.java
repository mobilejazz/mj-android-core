package com.mobilejazz.core.common;

public interface Callback<T> {

  void onSuccess(T t);

  void onError(Throwable throwable);

  interface Progress extends Callback {

    void onProgress(int progress);
  }

}
