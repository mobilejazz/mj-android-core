package com.mobilejazz.core.common.executors;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

public class AndroidExecutors {

  public static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
  public static final int CORE_POOL_SIZE = CPU_COUNT + 1;
  public static final int MAX_POOL_SIZE = CPU_COUNT * 2 + 1;
  public static final long KEEP_ALIVE_TIME = 1L;

  private static final AndroidExecutors INSTANCE = new AndroidExecutors();

  private final Executor uiThread;

  private AndroidExecutors() {
    this.uiThread = new UIThreadExecutor();
  }

  public static Executor uiThread() {
    return INSTANCE.uiThread;
  }

  private static class UIThreadExecutor implements Executor {

    private final Handler handler;

    public UIThreadExecutor() {
      this.handler = new Handler(Looper.getMainLooper());
    }

    @Override public void execute(@NonNull Runnable command) {
      handler.post(command);
    }
  }

}
