package com.mobilejazz.core.common.futures;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.mobilejazz.core.common.executors.AndroidExecutors;

public final class AndroidFutures {

  private AndroidFutures() {
    throw new AssertionError("No instances!");
  }

  public static <V> void addCallbackMainThread(ListenableFuture<V> future, FutureCallback<? super V> callback) {
    Futures.addCallback(future, callback, AndroidExecutors.uiThread());
  }

}
