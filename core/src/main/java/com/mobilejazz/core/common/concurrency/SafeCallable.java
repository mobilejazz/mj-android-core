package com.mobilejazz.core.common.concurrency;

import java.util.concurrent.Callable;

public abstract class SafeCallable<V> implements Callable<V> {

  @Override public V call() {
    try {
      return safeCall();
    } catch (Throwable t) {
      return onExceptionThrown(t);
    }
  }

  protected abstract V safeCall() throws Throwable;

  protected abstract V onExceptionThrown(final Throwable t);

}
