package com.mobilejazz.core.application.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.mobilejazz.core.application.ui.presenter.Presenter;
import com.mobilejazz.core.application.ui.presenter.PresenterView;

public abstract class BaseActivity<P extends Presenter<V>, V extends PresenterView> extends AppCompatActivity {

  public static final int NO_LAYOUT = -1;

  private Unbinder unbinder;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    onCreateSetContentView();
    onCreateButterKnife();
    initializeDependencyInjector();
    initializeToolbar();
    initializePresenter();
    initializeViews();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    onDestroyButterKnife();
    onDestroyPresenter();
  }

  @Override protected void onPause() {
    super.onPause();
    onPausePresenter();
  }

  @Override protected void onResume() {
    super.onResume();
    onResumePresenter();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (upNavigationEnabled()) {
      int itemId = item.getItemId();
      switch (itemId) {
        case android.R.id.home:
          onBackPressed();
          break;
      }
    }

    return super.onOptionsItemSelected(item);
  }

  //region Abstracts methods
  protected abstract @LayoutRes int getLayoutId();

  protected abstract void initializePresenter();

  protected abstract void initializeDependencyInjector();

  protected abstract void initializeToolbar();

  protected abstract void initializeViews();

  protected abstract P getPresenter();

  protected abstract void onDisplayError(Throwable throwable);
  //endregion

  //region Private methods

  /**
   * Override to handle up navigation
   */
  public boolean upNavigationEnabled() {
    return false;
  }

  private void onCreateButterKnife() {
    unbinder = ButterKnife.bind(this);
  }

  private void onCreateSetContentView() {
    @LayoutRes int layoutId = getLayoutId();
    if (layoutId != NO_LAYOUT) {
      setContentView(layoutId);
    }
  }

  private void onDestroyButterKnife() {
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  private void onDestroyPresenter() {
    getPresenter().onDestroy();
  }

  private void onResumePresenter() {
    getPresenter().onResume();
  }

  private void onPausePresenter() {
    getPresenter().onPause();
  }
  //endregion

}
