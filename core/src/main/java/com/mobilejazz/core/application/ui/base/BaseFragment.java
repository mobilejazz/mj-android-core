package com.mobilejazz.core.application.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.mobilejazz.core.application.ui.presenter.Presenter;
import com.mobilejazz.core.application.ui.presenter.PresenterView;

public abstract class BaseFragment<P extends Presenter<V>, V extends PresenterView> extends Fragment {

  private static final int NO_LAYOUT = -1;

  private Unbinder unbinder;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initializeDependencyInjector();
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    int layoutId = getLayoutId();
    if (layoutId != NO_LAYOUT) {
      final View rootView = inflater.inflate(layoutId, container, false);
      onCreateViewButterKnife(rootView);
      initializeViews(rootView);
      initializePresenter();
      return rootView;
    }
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override public void onResume() {
    super.onResume();
    getPresenter().onResume();
  }

  @Override public void onPause() {
    getPresenter().onPause();
    super.onPause();
  }

  @Override public void onDestroy() {
    getPresenter().onDestroy();
    onDestroyButterKnife();
    super.onDestroy();
  }

  //region Public methods
  public void onDisplayError(Throwable throwable) {
    BaseActivity<?, ?> baseActivity = getBaseActivity();
    if (baseActivity != null) {
      baseActivity.onDisplayError(throwable);
    }
  }
  //endregion

  //region Private methods
  private BaseActivity<?, ?> getBaseActivity() {
    FragmentActivity activity = getActivity();
    if (activity != null && !activity.isFinishing()) {
      return (BaseActivity<?, ?>) activity;
    }
    return null;
  }

  private void onDestroyButterKnife() {
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  private void onCreateViewButterKnife(View rootView) {
    unbinder = ButterKnife.bind(this, rootView);
  }
  //endregion

  //region Abstracts methods
  protected abstract @LayoutRes int getLayoutId();

  protected abstract void initializePresenter();

  protected abstract void initializeDependencyInjector();

  protected abstract void initializeViews(View rootView);

  protected abstract P getPresenter();
  //endregion
}
