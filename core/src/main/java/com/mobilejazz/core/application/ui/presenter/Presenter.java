package com.mobilejazz.core.application.ui.presenter;

public abstract class Presenter<T extends PresenterView> {

  protected final T view;

  public Presenter(T view) {
    this.view = view;
  }

  abstract public void onCreate();

  abstract public void onResume();

  abstract public void onPause();

  abstract public void onDestroy();

}
