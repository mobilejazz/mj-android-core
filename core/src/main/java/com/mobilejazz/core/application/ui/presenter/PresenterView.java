package com.mobilejazz.core.application.ui.presenter;

public interface PresenterView {

  void onDisplayError(Throwable throwable);

}
