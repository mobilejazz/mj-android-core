package com.mobilejazz.core.example;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.mobilejazz.core.domain.interactor.InteractorHandler;
import org.javatuples.Triplet;

import java.util.*;
import java.util.concurrent.*;

public class FutureExample {

  private final InteractorHandler interactorHandler;
  private final GetFooInteractor getFooInteractor;

  private final SearchFoosByAuthorInteractor searchFoosByAuthorInteractor;
  private final SearchFoosByTitleInteractor searchFoosByTitleInteractor;

  private final GetBarsInteractor getBarsInteractor;
  private final GetMostPopularFoosInteractor getMostPopularFoosInteractor;

  public FutureExample(final InteractorHandler interactorHandler,
      final GetFooInteractor getFooInteractor,
      final SearchFoosByAuthorInteractor searchFoosByAuthorInteractor,
      final SearchFoosByTitleInteractor searchFoosByTitleInteractor,
      final GetBarsInteractor getBarsInteractor,
      final GetMostPopularFoosInteractor getMostPopularFoosInteractor) {
    this.interactorHandler = interactorHandler;
    this.getFooInteractor = getFooInteractor;
    this.searchFoosByAuthorInteractor = searchFoosByAuthorInteractor;
    this.searchFoosByTitleInteractor = searchFoosByTitleInteractor;
    this.getBarsInteractor = getBarsInteractor;
    this.getMostPopularFoosInteractor = getMostPopularFoosInteractor;
  }

  public void main() {
    example_getValueFromSimpleInteractorInMainThread();

    example_getValueFromSimpleInteractorInBackgroundThread();

    example_searchSameResponseTime();

    example_multipleInteractorsResponsesWithDifferentsTypes();

    example_combineMultipleInteractorResponsesWithDifferentTypes();
  }

  /**
   * Example: Explain how to get the values from a interactor and the response is gonna be in main thread.
   */
  private void example_getValueFromSimpleInteractorInMainThread() {
    ListenableFuture<Optional<Foo>> getFooLf = getFooInteractor.execute(12);

    interactorHandler.addCallbackMainThread(getFooLf, new FutureCallback<Optional<Foo>>() {
      @Override public void onSuccess(final Optional<Foo> result) {
        // This code is executed in main thread

        if (result.isPresent()) {
          // execute the logic here
          Foo foo = result.get();
        }
      }

      @Override public void onFailure(final Throwable t) {
        // return the error to the view or just execute the logic here
      }
    });
  }

  /**
   * Example: Explain how to get the values from a interactor and the response is gonna be in background.
   */
  private void example_getValueFromSimpleInteractorInBackgroundThread() {
    ListenableFuture<Optional<Foo>> getFooLf = getFooInteractor.execute(12);
    interactorHandler.addCallback(getFooLf, new FutureCallback<Optional<Foo>>() {
      @Override public void onSuccess(final Optional<Foo> result) {
        // This code is executed in background thread.

        if (result.isPresent()) {
          Foo foo = result.get();
        }
      }

      @Override public void onFailure(final Throwable t) {
        // Your code
      }
    });
  }

  /**
   * Example: how to call to more than one interactor with the same response type and
   * combine the response in a single one.
   */
  private void example_searchSameResponseTime() {
    final ListenableFuture<Optional<List<Foo>>> getFoosByAuthorLf =
        searchFoosByAuthorInteractor.execute("jose");
    final ListenableFuture<Optional<List<Foo>>> getFoosByTitleLf =
        searchFoosByTitleInteractor.execute("jose");

    Callable<Optional<List<Foo>>> combiner = new Callable<Optional<List<Foo>>>() {
      @Override public Optional<List<Foo>> call() throws Exception {
        // Get the value directly from the ListenableFuture<Optional<List<Foo>>>
        Optional<List<Foo>> getFoosByAuthorOp = getFoosByAuthorLf.get();
        Optional<List<Foo>> getFoosByTitleOp = getFoosByTitleLf.get();

        List<Foo> foos = new ArrayList<>();

        if (getFoosByAuthorOp.isPresent()) {
          foos.addAll(getFoosByAuthorOp.get());
        }

        if (getFoosByTitleOp.isPresent()) {
          foos.addAll(getFoosByTitleOp.get());
        }

        return Optional.of(foos);
      }
    };

    // You could use Futures.whenAllComplete() or Futures.whenAllSucceed() methods depending of the
    // behaviour that you want. To see the differences go inside the method and read the documentation.
    ListenableFuture<Optional<List<Foo>>> getSearchesCombinedLf =
        Futures.whenAllComplete(getFoosByAuthorLf, getFoosByTitleLf).call(combiner);

    interactorHandler.addCallbackMainThread(getSearchesCombinedLf,
        new FutureCallback<Optional<List<Foo>>>() {
          @Override public void onSuccess(final Optional<List<Foo>> result) {
            if (result.isPresent()) {
              // execute your code
            }
          }

          @Override public void onFailure(final Throwable t) {
            // execute your code
          }
        });
  }

  /**
   * Example: How to call to multiples interactors with different response types.
   */
  private void example_multipleInteractorsResponsesWithDifferentsTypes() {
    final ListenableFuture<Optional<List<Bar>>> getBarsLf = getBarsInteractor.execute();
    final ListenableFuture<Optional<List<Foo>>> getMostPopularFoosLf =
        getMostPopularFoosInteractor.execute();

    interactorHandler.addCallbackMainThread(getBarsLf, new FutureCallback<Optional<List<Bar>>>() {
      @Override public void onSuccess(final Optional<List<Bar>> result) {
        if (result.isPresent()) {
          List<Bar> bars = result.get();

          // execute your code
        }
      }

      @Override public void onFailure(final Throwable t) {
        // execute your code
      }
    });

    interactorHandler.addCallbackMainThread(getMostPopularFoosLf,
        new FutureCallback<Optional<List<Foo>>>() {
          @Override public void onSuccess(final Optional<List<Foo>> result) {
            if (result.isPresent()) {
              List<Foo> foos = result.get();

              // execute your code
            }
          }

          @Override public void onFailure(final Throwable t) {
            // execute your code
          }
        });
  }

  /**
   * Example: How to call to multiples interactors with differents response types and combine it in a
   * single response.
   */
  private void example_combineMultipleInteractorResponsesWithDifferentTypes() {
    final ListenableFuture<Optional<List<Bar>>> getBarsLf = getBarsInteractor.execute();
    final ListenableFuture<Optional<List<Foo>>> getMostPopularFoosLf =
        getMostPopularFoosInteractor.execute();
    final ListenableFuture<Optional<Foo>> getFooLf = getFooInteractor.execute(123);

    Callable<Triplet<Optional<List<Bar>>, Optional<List<Foo>>, Optional<Foo>>> combine =
        new Callable<Triplet<Optional<List<Bar>>, Optional<List<Foo>>, Optional<Foo>>>() {
          @Override public Triplet<Optional<List<Bar>>, Optional<List<Foo>>, Optional<Foo>> call()
              throws Exception {

            Optional<List<Bar>> getBarsOp = getBarsLf.get();
            Optional<List<Foo>> getMostPopularFoosOp = getMostPopularFoosLf.get();
            Optional<Foo> getFooOp = getFooLf.get();

            return new Triplet<>(getBarsOp, getMostPopularFoosOp, getFooOp);
          }
        };

    ListenableFuture<Triplet<Optional<List<Bar>>, Optional<List<Foo>>, Optional<Foo>>> combineLf =
        Futures.whenAllSucceed(getBarsLf, getMostPopularFoosLf, getFooLf).call(combine);

    interactorHandler.addCallbackMainThread(combineLf,
        new FutureCallback<Triplet<Optional<List<Bar>>, Optional<List<Foo>>, Optional<Foo>>>() {
          @Override public void onSuccess(
              final Triplet<Optional<List<Bar>>, Optional<List<Foo>>, Optional<Foo>> result) {
            Optional<List<Bar>> getBarsOp = result.getValue0();
            Optional<List<Foo>> getMostPopularFoosOp = result.getValue1();
            Optional<Foo> getFooOp = result.getValue2();

            if (getBarsOp.isPresent()) {
              List<Bar> bars = getBarsOp.get();

              // Execute your code
            }

            if (getMostPopularFoosOp.isPresent()) {
              List<Foo> getMostPopularFoos = getMostPopularFoosOp.get();

              // Execute your code
            }

            if (getFooOp.isPresent()) {
              Foo foo = getFooOp.get();

              // Execute your code
            }
          }

          @Override public void onFailure(final Throwable t) {
            // execute your code
          }
        });
  }

  //region Examples
  //endregion

  class SearchFoosByAuthorInteractor {

    ListenableFuture<Optional<List<Foo>>> execute(String author) {
      return Futures.immediateFuture(Optional.of(Collections.<Foo>emptyList()));
    }
  }

  class SearchFoosByTitleInteractor {

    ListenableFuture<Optional<List<Foo>>> execute(String title) {
      return Futures.immediateFuture(Optional.of(Collections.<Foo>emptyList()));
    }
  }

  class GetFooInteractor {

    ListenableFuture<Optional<Foo>> execute(int id) {
      return Futures.immediateFuture(Optional.of(new Foo()));
    }
  }

  class GetMostPopularFoosInteractor {

    ListenableFuture<Optional<List<Foo>>> execute() {
      return Futures.immediateFuture(Optional.of(Collections.<Foo>emptyList()));
    }
  }

  class GetBarsInteractor {

    ListenableFuture<Optional<List<Bar>>> execute() {
      return Futures.immediateFuture(Optional.of(Collections.<Bar>emptyList()));
    }
  }

  class Foo {

  }

  class Bar {

  }
}
